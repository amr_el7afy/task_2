import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: "Task2",
    home: myApp(),
    debugShowCheckedModeBanner: false,
  ));
}
class myApp extends StatefulWidget {
  @override
  _myAppState createState() => _myAppState();
}

class _myAppState extends State<myApp> {
  Widget_social(Image imagee){
    return Container(
      child: imagee,
      margin: EdgeInsets.only(top: 35,left: 55,bottom: 40),
      width: 80,
      height: 80,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
      ),
    );
  }
Widget _textform(String hint,bool secure){
return Padding(padding: EdgeInsets.only(bottom: 20,left: 30,right: 30),
      child: TextFormField(
        obscureText: secure,
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(fontSize: 20),
                  ),
      ),
  );
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Padding(padding: EdgeInsets.only(bottom: 100,top: 20),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[Text('SIGN IN',style: TextStyle(fontSize: 40,
                  fontWeight: FontWeight.w700),
              )
              ],
            )
            ,),
          _textform("Username or Email", false),
          _textform("Password", true),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Forgot your password?',style: TextStyle(color: Colors.blue,fontSize: 17),)
            ],
          ),
          Container(
            margin: EdgeInsets.only(left: 35,right: 35,top: 25,bottom:40),
             height: 65,
             decoration: BoxDecoration(
               borderRadius: BorderRadius.circular(30),
               color: Colors.blue
             ),
            child: Center(
              child: Text('Submit',style: TextStyle(color: Colors.white,fontSize: 21),),
            ),
          ),
          Center(
            child:Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Sign in with",style: TextStyle(fontSize: 17),)
              ],
            )
          ),
          Center(
            child: Row(
              children: <Widget>[
                Widget_social(Image.asset('assets/facebook.png')),
                Widget_social(Image.asset('assets/twitter.png')),
                Widget_social(Image.asset('assets/google.png'))

              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Don't have an account?",style: TextStyle(fontSize: 17))
              ,Text("sign up",style: TextStyle(color: Colors.blue,fontSize: 17),)
            ],
          )
        ],
      ),
    );
  }
}
